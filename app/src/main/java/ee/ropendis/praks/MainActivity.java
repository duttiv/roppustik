package ee.ropendis.praks;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private List<String> verbList;
    private List<String> nounList;
    private List<String> adjList;
    private List<String> subjList;
    private List<String> colourList;
    private String[] roppusTypes = new String[]{"verb", "noun", "adj", "subj"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateVocabulary();
        writePhrase("Roppus-placeholder");
        initPhraseButton();
        initSpinnerValues();
        initAddRoppusBtn();
        initShowRoppusListBtn();
    }

    private void initShowRoppusListBtn() {
        Button showRoppusListBtn = (Button) findViewById(R.id.showRoppusList);
        showRoppusListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRoppusList();
            }
        });
    }

    private void goToRoppusList() {
        Intent intent = new Intent(this, RoppusListActivity.class);
        startActivity(intent);
    }

    private void initAddRoppusBtn() {
        Button addRoppus = (Button) findViewById(R.id.addRoppusBtn);
        addRoppus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText roppusInput = (EditText) findViewById(R.id.roppusInput);
                String roppus = roppusInput.getText().toString();
                Spinner roppusTypeInput = (Spinner) findViewById(R.id.roppusTypeInput);
                String roppusType = roppusTypeInput.getSelectedItem().toString();
                addWordToList(roppus, roppusType);
            }
        });
    }

    private void addWordToList(String word, String wordType) {
        switch (wordType) {
            case "verb":
                verbList.add(word);
                break;
            case "noun":
                nounList.add(word);
                break;
            case "adj":
                adjList.add(word);
                break;
            case "subj":
                subjList.add(word);
                break;
        }
        Toast.makeText(this, "Lisatud " + wordType + ": " + word,
                Toast.LENGTH_SHORT).show();
    }

    private void initSpinnerValues() {
        Spinner spinner = (Spinner) findViewById(R.id.roppusTypeInput);
        spinner.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, roppusTypes));
    }

    private void generateVocabulary() {
        verbList = new ArrayList<>();
        verbList.add("Istu");
        verbList.add("Astu");
        verbList.add("Kuku");
        verbList.add("Konfi");
        verbList.add("Näksi");
        verbList.add("Mine");
        verbList.add("Hüppa");
        nounList = new ArrayList<>();
        nounList.add("sisse");
        nounList.add("tasku");
        nounList.add("kella");
        nounList.add("ruuterit");
        nounList.add("larfi");
        nounList.add("püksi");
        adjList = new ArrayList<>();
        adjList.add("Lambi");
        adjList.add("Ülo");
        adjList.add("Tasku");
        adjList.add("Mäla");
        adjList.add("Jorni");
        adjList.add("Larfi");
        adjList.add("Kella");
        subjList = new ArrayList<>();
        subjList.add("Ülo");
        subjList.add("Peeter");
        subjList.add("Vello");
        subjList.add("ILDF");
        subjList.add("Lombok");
        subjList.add("Meeter");
        colourList = new ArrayList<>();
        colourList.add("#e31e50");
        colourList.add("#e5781d");
        colourList.add("#7ee51d");
        colourList.add("#1daee5");
        colourList.add("#a300ff");
    }

    private void writePhrase(String inputText) {
        TextView phraseContainer = (TextView) findViewById(R.id.roppus);
        phraseContainer.setText(inputText);
        Random random = new Random();
        String colourHex = colourList.get(random.nextInt(colourList.size()));
        phraseContainer.setTextColor(Color.parseColor(colourHex));
    }

    private void initPhraseButton() {
        Button phraseButton = (Button) findViewById(R.id.roppusBtn);
        phraseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phrase = generatePhrase();
                writePhrase(phrase);
                saveToPreferences(phrase);
            }
        });
    }

    private void saveToPreferences(String phrase) {
        SharedPreferences sharedPreferences = getSharedPreferences("RoppusStorage", MODE_PRIVATE);
        Set<String> myRoppusSet = sharedPreferences.getStringSet("MyRoppusSet", new HashSet<String>());
        myRoppusSet.add(phrase);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet("MyRoppusSet", myRoppusSet);
        editor.apply();
    }

    private String generatePhrase() {
        String phrase = "{verb} {noun}, {adj}-{subj}!";
        Random random = new Random();
        phrase = phrase.replace("{verb}", verbList.get(random.nextInt(verbList.size())));
        phrase = phrase.replace("{noun}", nounList.get(random.nextInt(nounList.size())));
        phrase = phrase.replace("{adj}", adjList.get(random.nextInt(adjList.size())));
        phrase = phrase.replace("{subj}", subjList.get(random.nextInt(subjList.size())));
        return phrase;
    }
}
