package ee.ropendis.praks;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RoppusListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roppus_list);
        showPhrasesInList();
    }

    private Set<String> getPhrasesFromPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("RoppusStorage", MODE_PRIVATE);
        return sharedPreferences.getStringSet("MyRoppusSet", new HashSet<String>());
    }

    private void showPhrasesInList() {
        ListView roppusListView = (ListView) findViewById(R.id.roppusList);
        List<String> roppusList = new ArrayList<>(getPhrasesFromPreferences());
        ArrayAdapter<String> roppusAdapter
                = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, roppusList);
        roppusListView.setAdapter(roppusAdapter);
    }

}
